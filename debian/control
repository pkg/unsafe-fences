Source: unsafe-fences
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Miguel Landaeta <nomadium@debian.org>
Build-Depends: debhelper (>= 10~),
               default-jdk (>= 2:1.8~),
               libmaven-javadoc-plugin-java,
               maven-debian-helper
Standards-Version: 3.9.8
Homepage: https://github.com/headius/unsafe-fences
Vcs-Git: https://anonscm.debian.org/git/pkg-java/unsafe-fences.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-java/unsafe-fences.git

Package: libunsafe-fences-java
Architecture: all
Depends: ${maven:Depends},
         ${misc:Depends}
Description: wrapper library around the Java 8 fences API
 unsafe-fences is a very simple Java library that just provides a shim
 around the Java 8 Unsafe methods used for memory fencing.
 .
 In Java 8, three memory-ordering intrinsics were added to the
 sun.misc.Unsafe class: fullFence, storeFence, and loadFence.
 .
 The main goal of unsafe-fences is to allow code in Java 6 and 7
 to compile those calls with a provided boolean guard.
 .
 This is especially useful for projects like JRuby that support
 several JDK versions but it can be reused by other Java projects as
 well.
